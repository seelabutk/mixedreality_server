MAKEFLAGS += --warn-undefined-variables
SHELL := bash
.SHELLFLAGS := -eu -o pipefail -c
.DEFAULT_GOAL := all
.DELETE_ON_ERROR:
.SUFFIXES:

################
# Utilities

# $(call make-lazy FOO) will turn FOO into a variable that, when accessed, will
# evaluate its definition once and then remember that value for the rest of the
# makefile. For example:
#
#   FOO = $(shell date +%s)
#   $(call make-lazy FOO)
#   t0 := $(FOO)
#   $(shell sleep 3)
#   t1 := $(FOO)
#
# Without the make-lazy call, $(t0) and $(t1) will have different values, but
# with it, they will have the same values.
#
# $(1) = name of variable to make lazy
make-lazy = $(eval $1 = $​$(eval $1 := $(value $(1)))$​$($1))

# Used for backups
date := $(shell date +%Y%m%d%H%M%S)

################
# Environment variables

PYTHON := python3
RUN_COMMAND :=

################
# Sanity checks and local variables

ifneq ($(shell $(PYTHON) -m venv &>/dev/null; echo $$?), 1)
venv_command := $(PYTHON) -m venv
else ifneq ($(shell pyvenv &>/dev/null; echo $$?), 127)
venv_command := pyvenv
else
$(error no venv command found)
endif

ifneq ($(shell $(PYTHON) -m pip &>/dev/null; echo $$?), 1)
pip_command := $(PYTHON) -m pip
else ifneq ($(shell pip3 &>/dev/null; echo $$?), 127)
pip_command := pip3
else
$(error no pip command found)
endif

define venv
set +u && source venv/bin/activate && set -u &&
endef

################
# Exported variables

export DATE := $(date)

################
# Standard targets

.PHONY: all
all:

.PHONY: check
check:

.PHONY: depend
depend: .depend.secondary

.PHONY: run
run: .depend.secondary
ifeq ($(RUN_COMMAND),)
	$(venv) $(PYTHON) manage.py makemigrations
	$(venv) $(PYTHON) manage.py migrate
	$(venv) $(PYTHON) manage.py runserver '0.0.0.0:8888'
else
	$(venv) $(RUN_COMMAND)
endif

.PHONY: clean
clean:
	find . -name '*~' -exec rm -v -- {} \+

################
# Apilication specific targets

################
# Source transformations

venv:
	$(venv_command) $@

.depend.secondary: requirements.txt | venv
	$(venv) $(pip_command) install -r $<
	touch $@

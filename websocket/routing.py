from channels.routing import route
from websocket.consumers import ws_connect, ws_receive, ws_disconnect


channel_routing = [
    route('websocket.connect', ws_connect, path=r'/(?P<room>[0-9]+)'),
    route('websocket.receive', ws_receive, path=r'/(?P<room>[0-9]+)'),
    route('websocket.disconnect', ws_disconnect, path=r'/(?P<room>[0-9]+)'),
]

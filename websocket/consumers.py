from channels import Group
from channels.sessions import channel_session
from uuid import uuid4, UUID
import json
import csv


SERVER_UUID = UUID(int=0)


@channel_session
def ws_connect(message, room):
    message.channel_session['id'] = str(uuid4())

    message.reply_channel.send({
        'text': json.dumps({
            'type': 'id/set',
            'origin': str(SERVER_UUID),
            'payload': {
                'id': message.channel_session['id'],
            },
        }),
    })

    Group('room-%s' % room).add(message.reply_channel)


@channel_session
def ws_receive(message, room):
    print('>>>{}<<<'.format(message.content['text']))
    data = json.loads(message.content['text'])

    if data['type'] == 'data/request':
        species_data = []
        with open('/home/tanner/src/bitbucket.org/luad/mixedreality/data/species/' + data['payload']['dataset'] + '.csv') as f:
            reader = csv.DictReader(f)
            for row in reader:
                species_data.append(row)

        Group('room-%s' % room).send({
            'text': json.dumps({
                'type': 'data/set',
                'origin': str(SERVER_UUID),
                'payload': {
                    'points': species_data,
                },
            }),
        })

    else:
        Group('room-%s' % room).send({
            'text': json.dumps({
                'type': data['type'],
                'origin': data.get('origin', message.channel_session['id']),
                'payload': data['payload'],
            }),
        })


@channel_session
def ws_disconnect(message, room):
    Group('room-%s' % room).discard(message.reply_channel)

    Group('room-%s' % room).send({
        'text': json.dumps({
            'type': 'device/disconnect',
            'origin': str(SERVER_UUID),
            'payload': {
                'id': message.channel_session['id'],
            },
        }),
    })

from channels.routing import include


channel_routing = [
    include('websocket.routing.channel_routing', path=r'^/websocket'),
]
